using System.Globalization;
using PTCExam.Helpers;

namespace PTCExam.Services;

public class ExamService
{
    private readonly DateTimeHelper _dateTimeHelper;
    public ExamService(DateTimeHelper dateTimeHelper)
    {
        _dateTimeHelper = dateTimeHelper;
    }
    
    public void PrintBarcode(string barcode)
    {
        if (string.IsNullOrEmpty(barcode))
        {
            throw new ArgumentNullException(nameof(barcode));
        }

        if (barcode.Length != 15)
        {
            throw new ArgumentOutOfRangeException(nameof(barcode));
        }

        var datetimeString = barcode.Substring(5, 10);

        if (DateTime.TryParseExact(datetimeString, 
                "yyyyMMddHH", 
                CultureInfo.InvariantCulture, 
                DateTimeStyles.None,
                out var expirationDate).Equals(false))
        {
            throw new ArgumentOutOfRangeException("barcode is not valid");
        }
        
        var localTime = _dateTimeHelper.Now();
        string hint;
        Console.WriteLine($"系統時間 {localTime.ToString("yyyy/MM/dd HH:mm")}");
        var expirationMinutes = expirationDate.Subtract(localTime).TotalMinutes;
        hint = expirationMinutes switch
        {
            <= 55 and >= 5 => "商品即將到期",
            < 5 => "商品已過期",
            _ => "商品尚未到期"
        };
        Console.WriteLine($"{barcode}        {hint}");
    }
}