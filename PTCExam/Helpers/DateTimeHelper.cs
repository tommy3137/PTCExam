namespace PTCExam.Helpers;

/// <summary>
/// 時間相關的Helper
/// </summary>
public class DateTimeHelper
{
    /// <summary>
    /// 取得現在時間
    /// </summary>
    /// <returns></returns>
    public virtual DateTime Now()
    {
        return DateTime.Now;
    }
    
    /// <summary>
    /// 取得現在時間(含時區)
    /// </summary>
    /// <returns></returns>
    public virtual DateTimeOffset NowOffset()
    {
        return DateTimeOffset.Now;
    }
}