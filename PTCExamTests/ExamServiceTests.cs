using Moq;
using PTCExam.Helpers;
using PTCExam.Services;

namespace PTCExamTests;

public class ExamServiceTests
{
    [Fact]
    public void ExamService_輸入正確條碼_若已過期_印出商品已過期()
    {
        // Arrange
        var dateTimeHelper = new Mock<DateTimeHelper>();
        dateTimeHelper.Setup(x => x.Now()).Returns(new DateTime(2023, 10, 15, 12, 58, 10));
        var examService = new ExamService(dateTimeHelper.Object);
        var barcode = "ABCDE2023101513";
        var consoleOutput = new StringWriter();
        Console.SetOut(consoleOutput);
        
        var expected = "系統時間 2023/10/15 12:58\nABCDE2023101513        商品已過期\n";
        // Act
        examService.PrintBarcode(barcode);
        
        // Assert
        Assert.Equal(expected, consoleOutput.ToString());
    }
    
    [Fact]
    public void ExamService_輸入正確條碼_若即將過期_印出商品即將到期()
    {
        // Arrange
        var dateTimeHelper = new Mock<DateTimeHelper>();
        dateTimeHelper.Setup(x => x.Now()).Returns(new DateTime(2023, 10, 15, 12, 54, 10));
        var examService = new ExamService(dateTimeHelper.Object);
        var barcode = "ABCDE2023101513";
        var consoleOutput = new StringWriter();
        Console.SetOut(consoleOutput);
        
        var expected = "系統時間 2023/10/15 12:54\nABCDE2023101513        商品即將到期\n";
        // Act
        examService.PrintBarcode(barcode);
        
        // Assert
        Assert.Equal(expected, consoleOutput.ToString());
    }

    [Fact] 
    public void ExamService_輸入正確條碼_若尚未到期_印出商品尚未到期()
    {
        // Arrange
        var dateTimeHelper = new Mock<DateTimeHelper>();
        dateTimeHelper.Setup(x => x.Now()).Returns(new DateTime(2023, 10, 15, 11, 54, 10));
        var examService = new ExamService(dateTimeHelper.Object);
        var barcode = "ABCDE2023101513";
        var consoleOutput = new StringWriter();
        Console.SetOut(consoleOutput);
        
        var expected = "系統時間 2023/10/15 11:54\nABCDE2023101513        商品尚未到期\n";
        // Act
        examService.PrintBarcode(barcode);
        
        // Assert
        Assert.Equal(expected, consoleOutput.ToString());
    }
    
    [Fact] 
    public void ExamService_輸入空的條碼_拋出ArgumentNullException()
    {
        // Arrange
        var dateTimeHelper = new Mock<DateTimeHelper>();
        dateTimeHelper.Setup(x => x.Now()).Returns(new DateTime(2023, 10, 15, 11, 54, 10));
        var examService = new ExamService(dateTimeHelper.Object);
        var barcode = "";
        
        // Act
        // Assert
        Assert.Throws<ArgumentNullException>(() => examService.PrintBarcode(barcode));
    }
    
    [Fact] 
    public void ExamService_輸入條碼長度不是15_拋出ArgumentOutOfRangeException()
    {
        // Arrange
        var dateTimeHelper = new Mock<DateTimeHelper>();
        dateTimeHelper.Setup(x => x.Now()).Returns(new DateTime(2023, 10, 15, 11, 54, 10));
        var examService = new ExamService(dateTimeHelper.Object);
        var barcode = "ABCDE20231015133212";
        
        // Act
        // Assert
        Assert.Throws<ArgumentOutOfRangeException>(() => examService.PrintBarcode(barcode));
    }
    
    [Fact] 
    public void ExamService_輸入條碼格式不對_拋出ArgumentOutOfRangeException()
    {
        // Arrange
        var dateTimeHelper = new Mock<DateTimeHelper>();
        dateTimeHelper.Setup(x => x.Now()).Returns(new DateTime(2023, 10, 15, 11, 54, 10));
        var examService = new ExamService(dateTimeHelper.Object);
        var barcode = "ABCDEFGHIJKLMNO";
        
        // Act
        // Assert
        Assert.Throws<ArgumentOutOfRangeException>(() => examService.PrintBarcode(barcode));
    }
}